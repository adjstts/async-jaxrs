package corp.adjstts.asyncjaxrs;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

@Path("/ejb")
@Stateless
public class AsyncEJBResource
{
    @GET @Asynchronous
    public void asyncGet(@Suspended AsyncResponse response,
                         @QueryParam("client_name") String clientName)
    throws Exception
    {
        Thread.sleep(5 * 1000);
        response.resume("async EJB GET done, " + clientName);
    }
}
