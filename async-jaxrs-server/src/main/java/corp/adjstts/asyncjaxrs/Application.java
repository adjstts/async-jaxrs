package corp.adjstts.asyncjaxrs;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/rest")
public class Application extends javax.ws.rs.core.Application
{
}