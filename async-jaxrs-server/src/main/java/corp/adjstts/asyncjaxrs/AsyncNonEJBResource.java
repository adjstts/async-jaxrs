package corp.adjstts.asyncjaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

@Path("/non-ejb")
public class AsyncNonEJBResource
{
    @GET
    public void asyncGet(@Suspended AsyncResponse response)
    {
        response.resume("async non-EJB GET done (synchronously)");
    }
}
