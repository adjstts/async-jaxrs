package corp.adjstts.asyncjaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/sync-non-ejb")
public class SyncNonEJBResource
{
    @GET
    public String get()
    {
        return "sync non-ejb GET done";
    }
}
