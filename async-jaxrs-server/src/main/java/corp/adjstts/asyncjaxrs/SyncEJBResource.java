package corp.adjstts.asyncjaxrs;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/sync-ejb")
@Stateless
public class SyncEJBResource
{
    @GET
    public String get()
    {
        return "sync GET done";
    }
}
