package corp.adjstts.asyncjaxrs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * RESTEasy 3.0.6.Final Reference Guide
 * 48.1. JAX-RS 2.0 Client API
 * https://docs.jboss.org/resteasy/docs/3.0.6.Final/userguide/html_single/index.html#d4e2098
 */
public class ClientTest
{
    private Client client;

    @Before
    public void setUp()
    {
        client = ClientBuilder.newClient();
    }

    @Test
    public void testSyncEJBResource()
    {
        WebTarget target = target(TestURLs.SYNC_EJB);

        Response response = target.request().get();
        String value = response.readEntity(String.class);
        response.close();

        Assert.assertEquals("sync GET done", value);
    }

    @Test
    public void testAsyncEJBResource()
    {
        String clientName = "man";

        WebTarget target =
            target(TestURLs.ASYNC_EJB + "?client_name=" + clientName);

        Response response = target.request().get();
        String value = response.readEntity(String.class);
        response.close();

        Assert.assertEquals("async EJB GET done, " + clientName, value);
    }

    private WebTarget target(String pathPart)
    {
        return client.target(TestURLs.BASE + pathPart);
    }
}
