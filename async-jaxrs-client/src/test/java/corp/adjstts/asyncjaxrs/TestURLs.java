package corp.adjstts.asyncjaxrs;

class TestURLs
{
    static final String BASE =
        "http://localhost:8080/async-jaxrs-server-1.0-SNAPSHOT/rest";

    static final String SYNC_EJB = "/sync-ejb";

    static final String ASYNC_EJB = "/ejb";
}
