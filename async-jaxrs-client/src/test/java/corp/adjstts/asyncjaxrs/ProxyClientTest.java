package corp.adjstts.asyncjaxrs;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 * RESTEasy 3.0.6.Final Reference Guide
 * 48.2. Resteasy Proxy Framework
 * https://docs.jboss.org/resteasy/docs/3.0.6.Final/userguide/html_single/index.html#d4e2106
 *
 * Resteasy has a simple API based on Apache HttpClient. You generate a proxy
 * then you can invoke methods on the proxy. The invoked method gets translated
 * to an HTTP request based on how you annotated the method and posted to the
 * server.
 *
 * The framework also supports the JAX-RS locator pattern, but on the client
 * side. So, if you have a method annotated only with @Path, that proxy method
 * will return a new proxy of the interface returned by that method.
 *
 * It is generally possible to share an interface between the client and server.
 * In this scenario, you just have your JAX-RS services implement an annotated
 * interface and then reuse that same interface to create client proxies to
 * invoke on the client-side.
 * (Так, в Krypton JavaFX-клиент и веб-тесты ипсользуют прокси тех же
 * интерфейсов, которые реализуют REST-сервисы на сервере).
 */
public class ProxyClientTest
{
    @Test
    public void testSyncEJBResource()
    {
        SyncEJBResource syncEJBResource = target().proxy(SyncEJBResource.class);

        String value = syncEJBResource.get();

        Assert.assertEquals("sync GET done", value);
    }

    @Test
    public void testAsyncEJBResource()
    {
        String clientName = "man";

        AsyncEJBResource asyncEJBResource =
            target().proxy(AsyncEJBResource.class);

        String value = asyncEJBResource.get(clientName);

        Assert.assertEquals("async EJB GET done, " + clientName, value);
    }

    // Два интерфейса ниже можно было даже объединить в один, поставив каждому
    // методу соответствующий path. Необходимости писать интерфейсы, полностью
    // совпадающие с интерфейсами вызываемых веб-сервисов, нет.

    @Path(TestURLs.SYNC_EJB)
    private interface SyncEJBResource {
        @GET
        String get();
    }

    @Path(TestURLs.ASYNC_EJB)
    private interface AsyncEJBResource {

        // На сервере этот метод REST-сервиса асинхронный, т.е. один из его
        // параметров - @Suspended AsyncResponse. Так как он инжектится в метод
        // REST-сервиса контейнером, клиент вообще не должен знать об этом
        // параметре. Соответственно, интерфейс клиента такого REST-сервиса не
        // может быть идентичен интерфейсу REST-сервиса.
        @GET
        String get(@QueryParam("client_name") String clientName);
    }

    private ResteasyWebTarget target() {
        // Отличие ResteasyClient от Client в том, что ResteasyClient#target
        // возвращает сразу ResteasyWebTarget. Метод Client#target возвращает
        // WebTarget, который необходимо самостоятельно кастить в
        // ResteasyWebTarget. ResteasyWebTarget нам нужен потому, что он
        // содержит метод .proxy(), создающий клиентские прокси для тестируемых
        // здесь REST-сервисов.
        // Важно, что используемая здесь версия RESTEasy - 3.0.19.Final, как и в
        // Krypton. Потому что, например, в версии 4.0.0.Beta8, класс
        // ResteasyClientBuilder уже абстрактный и его нельзя инстанциировать,
        // как это делается ниже:
        return new ResteasyClientBuilder().build().target(TestURLs.BASE);
    }
}
